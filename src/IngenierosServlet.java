

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class IngenierosServlet
 */
@WebServlet("/gi")
public class IngenierosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public IngenierosServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
response.setCharacterEncoding("UTF-8");
		PrintWriter out=response.getWriter();
		
		
		HTML5.comienzo(out, "Gestión de Empleados");
		out.println("<h1>Formulario para dar de alta a un ingeniero</h1>");
		out.println("<form method=\"GET\" action=\"gi\">");
		out.println("<p>Nombre:<input type= \"text\" name=\"nombre\"></input>");
		out.println("<p>Num NSS:<input type= \"text\" name=\"nss\"></input>");
		out.println("<p>Salario:<input type= \"number\" name=\"sal\"></input>");
		out.println("<p>Especialidad:<input type= \"text\" name=\"esp\"></input>");
		out.println("<p><label for=\"dep\">Departamento</label>");
		out.println("<select name=\"dep\">");
		agregarDepartamentos(out);
		out.println("</select>");
		out.println("<p><input type= \"submit\"></input>");
		out.println("</form>");
		
		insertarEmple(out,request);
		HTML5.fin(out);
		
	}
	private void agregarDepartamentos(PrintWriter out) {
		try {
			Connection c=  DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados","root","practicas");
			Statement sql=c.createStatement();
			ResultSet rs=sql.executeQuery("select nombre from departamentos");
			while(rs.next()) {
				out.printf("<option>%s</option>\n",rs.getString("nombre"));
			}rs.close();
			c.close();
		}catch (SQLException e) {
			
		}
		}
	private void insertarEmple(PrintWriter out, HttpServletRequest request) {
	
		try {
			Connection c=  DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados","root","practicas");
			String usu=request.getParameter("nombre");
			String nss=request.getParameter("nss");
			String sal=request.getParameter("sal");
			String esp=request.getParameter("esp");
			String dep=request.getParameter("dep");
			
			PreparedStatement sql= c.prepareStatement("insert into empleados values(?,?,?");
			sql.setString(1, nss);
			sql.setString(2, usu);
			sql.setString(3, sal);
			
			sql.executeUpdate();
			PreparedStatement sql1= c.prepareStatement("insert into ingenieros values(?,?");
			sql1.setString(1, nss);
			sql1.setString(2, dep);
			
			sql1.executeUpdate();
			
			PreparedStatement sql2= c.prepareStatement("insert into especialidades values(?,?");
			sql2.setString(1, nss);
			sql2.setString(2, esp);
			
			sql2.executeUpdate();
			
			out.print("¡Ingeniero insertado!");


			

		}catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	



}
